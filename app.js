const express = require('express');
const port = 8000;
const db = require('./db/connnection.js') 

const app = express();

app.use(express.json())


app.post('/cadastrar', (req, res) => {
    const newmulta = req.body;
    const cmd_insert = 'INSERT INTO MULTA SET?'
    db.query(cmd_insert,newmulta,(error,result) => {
        if(error) {
            res.status(400).json({message:"Informações inseridas para multa inválidas " + error}) 
        }else{
            res.status(201).json({message:"Multa cadastrada com sucesso " + result.insertId}) 
        }
    })
})


app.get('/cadastrar/cpf/:cpf', (req, res) => {
    const buscacondutor = req.params.cpf
    const busca = 'SELECT CODIGOMULTA, CPF, PLACA, NOMEDOCLIENTE, PONTOS FROM MULTA WHERE CPF=?'


    db.query(busca,buscacondutor,(err, rows)=>{
        res.status(200).json(rows);
    })
})

app.put('/cadastrar/:codigomulta', (req, res) => {
    const codigomulta = req.params.codigomulta;
    const body = req.body;

    const sql = "UPDATE MULTA SET PLACA=? WHERE CODIGOMULTA=?"
    const values = [body.PLACA, codigomulta]
    
    db.query(sql, values,(error,result)=>{
        if (error) {
            res.status(400).json({message:"Informações inseridas é inválidas " + error})
        }else{
            res.status(201).json({message: "A placa foi alterada com sucesso " })
        }
    })  
})

/*PROFESSOR ESSE DELETE EU CRIE PORQUE NO EXERCICIO FALA PARA EXCLUIR SOMENTE A PLACA, FIQUEI COM DÚVIDA SE ERA PARA EXCLUIR A OU APENAS FAZER UM PUT DA PLACA, NO FINAL EU FIZ OS 2 PARA NÃO TER ERRO*/
app.delete('/cadastrar/:placa', (req, res) => {
    const codigomulta = req.params.placa;
    
    const sql = "DELETE FROM MULTA WHERE PLACA = ?"
    
    db.query(sql,codigomulta,(error,result)=>{
        if (error) {
            res.status(400).json({message:"Informações inseridas é inválidas " + error})
        }else{
            res.status(201).json({message: "A placa foi excluída com sucesso " })
        }
    })  
})

app.listen(port,()=>{
    console.log("------------------------------")
    console.log(`Rodando na porta ${port}`)
    console.log("------------------------------")
})